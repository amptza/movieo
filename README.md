# Movieo

**Movieo works on Node.js version 14.** Movieo is a small responsive React web application that searches for movies using RapidAPI's Movie Database Alternative API and lists search results in a selectable list or card grid layout - a user can toggle between these two views by clicking on the *List View* or *Grid View* button. The API call is done after 800ms have passed since the users last keystroke. Once a user clicks on a movie the web app displays more information about the movie in a modal. The user is able to add (and remove) movies to favourites from the modal and can toggle the favourites list between a list and card view as well. The user can toggle between the favourites list/grid and movies list/grid by clicking on the *View* and *Hide Favourites* button. State management was implemented using Easy Peasy (an abstraction of Redux) and React Bootstrap was used for UI components - both as required by the project specification.

## Node version

Movieo works on Node.js version 14. To change to this version of Node.js relatively easily, please consider using https://www.npmjs.com/package/n for MacOS, Linux and WSL2 and https://github.com/coreybutler/nvm-windows#overview for Windows.

## Install

**Movieo works on Node.js version 14.** Clone the repo and navigate to the root folder and run the following command to install all the dependencies:

```
$ npm install
```

An `.env` file contains the API key for RapidAPI and has been committed to this private repo for ease of use, even though it would not generally be committed to the repo.

It can be updated by setting it as follows:

```
REACT_APP_RAPID_API_KEY= 'enter your private key here'
```

## Usage

In the project directory, you can run the following commands:

### Start a development server

```
$ npm run start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.


### Build a production build

```
npm run build
```

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!


### Deployment

See the following section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


### Eject from create-react-app

```
npm run eject
```

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

#### License

MIT License
Copyright (c)
Ruan Botha