import ListGroup from "react-bootstrap/ListGroup";
import ListItem from "./ListItem";

const ListView = (props) => {
  const { movies, viewFavourites } = props;

  return (
    <ListGroup defaultActiveKey="#link1">
      {movies &&
        movies.map((movie, index) => (
          <ListItem key={movie.imdbID} movie={movie} viewFavourites={viewFavourites}></ListItem>
        ))}
    </ListGroup>
  );
};

export default ListView;
