import { useState, useEffect } from "react";
import { useStoreActions, useStoreState } from "easy-peasy";

import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";

const CardItem = (props) => {
  const { movie, viewFavourites } = props;
  const [isLoading, setIsLoading] = useState(false);
  const showMovieModal = useStoreState((state) => state.showMovieModal);
  const setShowMovieModal = useStoreActions(
    (actions) => actions.setShowMovieModal
  );
  const removeFromFavourites = useStoreActions(
    (actions) => actions.removeFromFavourites
  );

  useEffect(() => {
    if (!showMovieModal.isLoading) {
      setIsLoading(false);
    }
  }, [showMovieModal.isLoading]);

  const removeFromFavouritesHandler = (movie) => {
    removeFromFavourites(movie);
  };

  const showModalHandler = () => {
    if (showMovieModal.isLoading) {
      return 
    }
    setIsLoading(true);
    setShowMovieModal({
      show: true,
      imdbID: movie.imdbID,
      isFavourited: movie.isFavourited,
      isLoading: true,
    });
  };

  return (
    <Card onClick={showModalHandler}>
      <Card.Img
        variant="top"
        src={
          movie.Poster !== "N/A"
            ? movie.Poster
            : "https://upload.wikimedia.org/wikipedia/commons/6/65/No-Image-Placeholder.svg"
        }
      />
      <Card.Body>
        <Card.Title>{movie.Title}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">
          {movie.Year.charAt(movie.Year.length - 1) === "–"
            ? movie.Year +
              "current (" +
              movie.Type.charAt(0).toUpperCase() +
              movie.Type.slice(1) +
              ")"
            : movie.Year +
              " (" +
              movie.Type.charAt(0).toUpperCase() +
              movie.Type.slice(1) +
              ")"}
        </Card.Subtitle>
        {viewFavourites && (
          <Button
            variant="warning"
            onClick={(e) => {
              e.stopPropagation();
              removeFromFavouritesHandler(movie);
            }}
          >
            Remove from favourites
          </Button>
        )}
        {showMovieModal.isLoading && isLoading && (
          <Spinner animation="border" variant="primary" />
        )}
      </Card.Body>
    </Card>
  );
};

export default CardItem;
