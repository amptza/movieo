import { useState, useEffect, Fragment } from "react";
import { useStoreState, useStoreActions } from "easy-peasy";

import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

const MovieDetailsModal = (props) => {
  const [movieDetails, setMovieDetails] = useState(null);
  const showMovieModal = useStoreState((state) => state.showMovieModal);
  const setShowMovieModal = useStoreActions(
    (actions) => actions.setShowMovieModal
  );
  const addToFavourites = useStoreActions((actions) => actions.addToFavourites);
  const removeFromFavourites = useStoreActions(
    (actions) => actions.removeFromFavourites
  );

  useEffect(() => {
    const imdbID = showMovieModal.imdbID;
    fetch(
      `https://movie-database-alternative.p.rapidapi.com/?r=json&i=${imdbID}`,
      {
        method: "GET",
        headers: {
          "x-rapidapi-host": "movie-database-alternative.p.rapidapi.com",
          "x-rapidapi-key": process.env.REACT_APP_RAPID_API_KEY,
        },
      }
    )
      .then((response) => {
        response.json().then((data) => {
          setMovieDetails(data);
          setShowMovieModal({ ...showMovieModal, isLoading: false });
        });
      })
      .catch((err) => {
        console.error(err);
        setShowMovieModal({ ...showMovieModal, isLoading: false });
      });
    return () => {
      setMovieDetails(null);
    };
  }, [showMovieModal.imdbID]);

  const addToFavouritesHandler = (movieDetails) => {
    addToFavourites(movieDetails);
    setShowMovieModal({
      show: true,
      imdbID: movieDetails.imdbID,
      isFavourited: true,
    });
  };

  const removeFromFavouritesHandler = (movieDetails) => {
    removeFromFavourites(movieDetails);
    setShowMovieModal({
      show: true,
      imdbID: movieDetails.imdbID,
      isFavourited: false,
    });
  };

  return (
    <Fragment>
      {movieDetails && (
        <Modal
          {...props}
          size="lg"
          aria-labelledby="contained-modal-title-hcenter"
          centered
          className="movie-details"
        >
          <Row>
            <Col md={6}>
              <img
                alt="movie poster"
                src={
                  movieDetails.Poster !== "N/A"
                    ? movieDetails.Poster
                    : "https://upload.wikimedia.org/wikipedia/commons/6/65/No-Image-Placeholder.svg"
                }
                style={{ width: "100%" }}
              />
            </Col>
            <Col md={6}>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                  <h4>{movieDetails.Title}</h4>
                  <h6 className="mb-2 text-muted">
                    {movieDetails.Year.charAt(movieDetails.Year.length - 1) ===
                    "–"
                      ? movieDetails.Year +
                        "current (" +
                        movieDetails.Type.charAt(0).toUpperCase() +
                        movieDetails.Type.slice(1) +
                        ")"
                      : movieDetails.Year +
                        " (" +
                        movieDetails.Type.charAt(0).toUpperCase() +
                        movieDetails.Type.slice(1) +
                        ")"}
                  </h6>
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <p>
                  {movieDetails.Plot !== "N/A"
                    ? movieDetails.Plot
                    : "No plot available for this title."}
                </p>
                <p>
                  <strong>Actors:</strong> {movieDetails.Actors} <br />
                  <strong>Rating:</strong> {movieDetails.imdbRating} <br />
                  <strong>Genre:</strong> {movieDetails.Genre} <br />
                  <strong>Runtime:</strong> {movieDetails.Runtime}
                </p>
                {!showMovieModal.isFavourited ? (
                  <Button
                    variant="warning"
                    onClick={() => addToFavouritesHandler(movieDetails)}
                  >
                    Add to favourites
                  </Button>
                ) : (
                  <Button
                    variant="warning"
                    onClick={() => removeFromFavouritesHandler(movieDetails)}
                  >
                    Remove from favourites
                  </Button>
                )}
              </Modal.Body>
            </Col>
          </Row>
        </Modal>
      )}
    </Fragment>
  );
};

export default MovieDetailsModal;
