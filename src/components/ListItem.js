import { useState, useEffect } from "react";
import ListGroup from "react-bootstrap/ListGroup";
import { useStoreActions, useStoreState } from "easy-peasy";
import Image from "react-bootstrap/Image";
import Spinner from "react-bootstrap/Spinner";

const ListItem = (props) => {
  const { movie, viewFavourites } = props;
  const [isLoading, setIsLoading] = useState(false);
  const showMovieModal = useStoreState((state) => state.showMovieModal);
  const setShowMovieModal = useStoreActions(
    (actions) => actions.setShowMovieModal
  );
  const removeFromFavourites = useStoreActions(
    (actions) => actions.removeFromFavourites
  );

  useEffect(() => {
    if (!showMovieModal.isLoading) {
      setIsLoading(false);
    }
  }, [showMovieModal.isLoading]);

  const removeFromFavouritesHandler = (movie) => {
    removeFromFavourites(movie);
  };

  const showModalHandler = () => {
    setIsLoading(true);
    setShowMovieModal({
      show: true,
      imdbID: movie.imdbID,
      isFavourited: movie.isFavourited,
      isLoading: true,
    });
  };

  return (
    <ListGroup.Item
      disabled={showMovieModal.isLoading}
      action
      onClick={showModalHandler}
    >
      <Image
        src={
          movie.Poster !== "N/A"
            ? movie.Poster
            : "https://upload.wikimedia.org/wikipedia/commons/6/65/No-Image-Placeholder.svg"
        }
        rounded
      />
      <div>
        <h5>{movie.Title}</h5>
        <h6 className="text-muted">
          {movie.Year.charAt(movie.Year.length - 1) === "–"
            ? movie.Year +
              "current (" +
              movie.Type.charAt(0).toUpperCase() +
              movie.Type.slice(1) +
              ")"
            : movie.Year +
              " (" +
              movie.Type.charAt(0).toUpperCase() +
              movie.Type.slice(1) +
              ")"}
        </h6>
        {viewFavourites && (
          <div
            className="btn btn-warning"
            onClick={(e) => {
              e.stopPropagation();
              removeFromFavouritesHandler(movie);
            }}
          >
            Remove from favourites
          </div>
        )}
        {showMovieModal.isLoading && isLoading && (
          <Spinner animation="border" variant="primary" />
        )}
      </div>
    </ListGroup.Item>
  );
};

export default ListItem;
