import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import CardItem from "./CardItem";

const CardGrid = (props) => {
  const { movies, viewFavourites } = props;

  return (
    <Row xs={1} md={4} className="g-4 card-grid">
      {movies &&
        movies.map((movie, index) => (
          <Col key={movie.imdbID}>
            <CardItem
              movie={movie}
              viewFavourites={viewFavourites}
            ></CardItem>
          </Col>
        ))}
    </Row>
  );
};

export default CardGrid;
