import { createStore, action, persist } from "easy-peasy";

const store = createStore(
  persist(
    {
      viewPreference: "list",
      movies: [],
      favourites: [],
      showMovieModal: {
        show: false,
        imdbID: null,
        isFavourited: false,
        isLoading: false,
      },
      setViewPreference: action((state, payload) => {
        state.viewPreference = payload;
      }),
      setShowMovieModal: action((state, payload) => {
        state.showMovieModal = payload;
      }),
      updateMovies: action((state, payload) => {
        if (!payload.movies) {
          return
        }
        if (payload.pageNumber < 2 || payload.movies.length === 0) {
          state.movies = payload.movies;
        } else {
          state.movies = state.movies.concat(payload.movies);
        }
        for (let i = 0; i < state.movies.length; i++) {
          let imdbIDOccurence = 0;
          for (let j = 0; j < state.movies.length; j++) {
            if (state.movies[i].imdbID === state.movies[j].imdbID) {
              if (imdbIDOccurence > 0) {
                state.movies.splice(i, 1);
                i--;
                break;
              } else {
                imdbIDOccurence++;
              }
            }
          }

          for (let j = 0; j < state.favourites.length; j++) {
            if (state.movies[i].imdbID === state.favourites[j].imdbID) {
              state.movies[i].isFavourited = true;
            }
          }
        }
      }),
      addToFavourites: action((state, payload) => {
        payload = { ...payload, isFavourited: true };
        state.favourites.push(payload);
        for (let i = 0; i < state.movies.length; i++) {
          if (state.movies[i].imdbID === payload.imdbID) {
            state.movies[i].isFavourited = true;
          }
        }
      }),
      removeFromFavourites: action((state, payload) => {
        for (let i = 0; i < state.favourites.length; i++) {
          if (state.favourites[i].imdbID === payload.imdbID) {
            state.favourites.splice(i, 1);
            for (let i = 0; i < state.movies.length; i++) {
              if (state.movies[i].imdbID === payload.imdbID) {
                state.movies[i].isFavourited = false;
              }
            }
          }
        }
      }),
    },
    {
      allow: ["favourites", "viewPreference"],
    }
  )
);

export default store;
