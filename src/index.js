import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import store from './store/store';
import { StoreProvider } from 'easy-peasy';

import './App.scss';

ReactDOM.render(
    <StoreProvider store={store}>
      <React.StrictMode>
          <App />
      </React.StrictMode>
    </StoreProvider>,
  document.getElementById('root')
);