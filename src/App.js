import React, { useState, useEffect, useRef } from "react";
import { useStoreState, useStoreActions } from "easy-peasy";

import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Alert from "react-bootstrap/Alert";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Spinner from "react-bootstrap/Spinner";

import MovieDetailsModal from "./components/MovieDetailsModal";
import CardGrid from "./components/CardGrid";
import ListView from "./components/ListView";

const App = () => {
  const [lastSearchString, setLastSearchString] = useState("");
  const [alertMessage, setAlertMessage] = useState("");
  const pageNumber = useRef(1);
  const totalResults = useRef(0);
  const [isLoading, setIsLoading] = useState(false);
  const [fetchMovieDataTimeOut, setFetchMovieDataTimeOut] = useState(null);
  const updateMovies = useStoreActions((actions) => actions.updateMovies);
  const viewPreference = useStoreState((state) => state.viewPreference);
  const setViewPreference = useStoreActions(
    (actions) => actions.setViewPreference
  );
  const movies = useStoreState((state) => state.movies);
  const favourites = useStoreState((state) => state.favourites);
  const showMovieModal = useStoreState((state) => state.showMovieModal);
  const setShowMovieModal = useStoreActions(
    (actions) => actions.setShowMovieModal
  );
  const [viewFavourites, setViewFavourites] = useState(false);

  useEffect(() => {
    if (observer) {
      observer.disconnect();
    }
    setObserver(lastSearchString);
  }, [viewPreference, viewFavourites]);

  let observer;
  let observerInstantiated = false;
  const fetchMovieData = (movieName, timeOutDuration) => {
    if (!timeOutDuration) { timeOutDuration = 900 }
    if (observer) {
      observer.disconnect();
    }
    clearTimeout(fetchMovieDataTimeOut);
    setFetchMovieDataTimeOut(
      setTimeout(() => {
        setIsLoading(true);
        fetch(
          `https://movie-database-alternative.p.rapidapi.com/?s=${movieName}&r=json&page=${pageNumber.current}`,
          {
            method: "GET",
            headers: {
              "x-rapidapi-host":
                "movie-database-alternative.p.rapidapi.com",
              "x-rapidapi-key": process.env.REACT_APP_RAPID_API_KEY,
            },
          }
        )
          .then((response) => {
            response.json().then((data) => {
              setViewFavourites(false);
              if (data.Error) {
                setAlertMessage(data.Error);
                setIsLoading(false);
                return
              }
              updateMovies({
                movies: data.Search,
                pageNumber: pageNumber.current,
              });
              totalResults.current = data.totalResults;

              if (pageNumber.current > data.totalResults / 10) {
                return setIsLoading(false);
              }

              setObserver(movieName);
              pageNumber.current = pageNumber.current + 1;
            });
          })
          .catch((err) => {
            setIsLoading(false);
            console.error(err);
            pageNumber.current = pageNumber.current - 1;
            alert("The search could not be completed at this time.");
          });
      }, timeOutDuration)
    );
  };

  const setObserver = (movieName) => {
    if (pageNumber.current > totalResults.current / 10) {
      return setIsLoading(false);
    }

    let options = {
      root: document.querySelector("document"),
      rootMargin: "0px",
      threshold: 0.1,
    };
    observer = new IntersectionObserver((entries) => {
      if (!observerInstantiated) {
        return (observerInstantiated = true);
      }
      if (!entries[0].isIntersecting) {
        return
      }
      if (!isLoading) {
        fetchMovieData(movieName, 200);
      }
    }, options);
    let target;
    if (viewPreference === "list") {
      target = document.querySelector(".list-group > button:last-child");
    } else if (viewPreference === "grid") {
      target = document.querySelector(".card-grid > div:last-child");
    }
    if (target) {
      observer.observe(target);
    }
    setIsLoading(false);
  };

  const onChangeHandler = (e) => {
    pageNumber.current = 1;
    const movieName = e.target.value.trimStart();
    e.target.value = movieName;
    setAlertMessage("");
    updateMovies({ movies: [], pageNumber: pageNumber.current });
    fetchMovieData(movieName);
    setLastSearchString(movieName);
  };

  const toggleViewFavourites = () => {
    setViewFavourites(!viewFavourites);
  };

  const toggleViewPreference = () => {
    if (viewPreference === "list") {
      setViewPreference("grid");
    } else {
      setViewPreference("list");
    }
  };

  return (
    <Container className="p-3">
      <Container className="pb-1 p-5 mb-4 bg-light rounded-3">
        <InputGroup className="mb-3">
          <FormControl
            aria-label="Default"
            aria-describedby="inputGroup-sizing-default"
            onChange={onChangeHandler}
            placeholder="Search for movies..."
          />
        </InputGroup>
        <div className="toggle-container">
          {isLoading && <Spinner animation="border" variant="primary" />}
          <Button
            className="view-favourites"
            variant="primary"
            onClick={toggleViewFavourites}
          >
            {!viewFavourites ? "View Favourites" : "Hide Favourites"}
          </Button>{" "}
          <Button className="toggle-view" onClick={toggleViewPreference}>
            {viewPreference === "list" ? "Grid View" : "List View"}
          </Button>
        </div>
        {alertMessage && (
          <Alert variant="danger">{alertMessage} Please try again...</Alert>
        )}
        {showMovieModal.imdbID && (
          <MovieDetailsModal
            show={showMovieModal.show}
            onHide={() => setShowMovieModal({ show: false, imdbID: null })}
          />
        )}

        {viewPreference === "list" ? (
          <ListView
            movies={!viewFavourites ? movies : favourites}
            viewFavourites={viewFavourites}
          ></ListView>
        ) : (
          <CardGrid
            movies={!viewFavourites ? movies : favourites}
            viewFavourites={viewFavourites}
          ></CardGrid>
        )}
        <div className="spinner-container">
          {isLoading && movies.length > 0 && (
            <Spinner animation="border" variant="primary" />
          )}
        </div>
      </Container>
    </Container>
  );
};

export default App;
